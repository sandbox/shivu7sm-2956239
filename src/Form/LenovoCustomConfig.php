<?php

namespace Drupal\len_custom\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LenovoCustomConfig.
 */
class LenovoCustomConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'len_custom.lenovocustomconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lenovo_custom_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('len_custom.lenovocustomconfig');
    // Generate a unique wrapper HTML ID.
    $ajax_wrapper_id = Html::getUniqueId('box-container');

    $form['production_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Production Configurations'),
    ];
    $form['staging_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Staging Configurations'),
    ];
    // Add a wrapper around the <form> with a unique ID. The content of this DOM
    // element will be replaced by whatever is returned from the Ajax request.
    $form['#prefix'] = '<div id="' . $ajax_wrapper_id . '">';
    $form['#suffix'] = '</div>';
    $form['production_fieldset']['content_publisher_update_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Content Publisher Update URL'),
      '#description' => $this->t('Content publisher update URL to be triggered from drupal. Used by update and create actions'),
      '#default_value' => $config->get('content_publisher_update_url'),
    ];
    $form['production_fieldset']['content_publisher_delete_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Content Publisher Delete URL'),
      '#description' => $this->t('Content publisher rest endpoint URL for delete actions on nodes'),
      '#default_value' => $config->get('content_publisher_delete_url'),
    ];
    $form['staging_fieldset']['content_publisher_update_url_staging'] = [
      '#type' => 'url',
      '#title' => $this->t('Content Publisher Update URL(Staging)'),
      '#description' => $this->t('Content publisher update URL to be triggered from drupal. Used by update and create actions'),
      '#default_value' => $config->get('content_publisher_update_url_staging'),
    ];
    $form['staging_fieldset']['content_publisher_delete_url_staging'] = [
      '#type' => 'url',
      '#title' => $this->t('Content Publisher Delete URL(Staging)'),
      '#description' => $this->t('Content publisher rest endpoint URL for delete actions on nodes'),
      '#default_value' => $config->get('content_publisher_delete_url_staging'),
    ];
    /*if ($form_state->has('string')) {
      // Build the content we want to display after the form was submitted.
      $form['box'] = [
        '#markup' => '<p>' . $this->t('You clicked submit on @date', ['@date' => date('c')]) . '</p>'
      ];

      $form['item'] = [
        '#markup' =>  '<p>' . t('You submitted %string which results in %reversed when reversed.', ['%string' => $form_state->get('string'), '%reversed' => $form_state->get('reversed_string')]) . '</p>',
      ];
    }
    // Otherwise this is the user first coming to the page so we need to
    // display the form in its initial state. Or, the form was submitted but
    // failed validation so we need to re-display the form and allow the user
    // to correct any errors.
    else {
      // The box contains some markup that we can change on a submit request.
      $form['box'] = [
        '#type' => 'markup',
        '#markup' => '<h1>Initial markup for box</h1>',
      ];

      // A plain textfield we can use to collect a value from the user.
      $form['item'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Enter some text'),
        '#description' => $this->t('Must be at least 5 characters.'),
      ];

      // The button for submitting the form. Configured to use Ajax to submit
      // the form via the #ajax property.
      $form['submit'] = [
        '#type' => 'submit',
        // Use the #ajax property to declare this button should trigger an Ajax
        // request.
        '#ajax' => [
          // The Ajax callback method that is responsible for responding to the
          // Ajax HTTP request.
          'callback' => '::promptCallback',
          // The ID of the DOM element whose content will be replaced with
          // whatever is returned from the above callback.
          'wrapper' => $ajax_wrapper_id,
        ],
        '#value' => $this->t('Submit'),
      ];
    }*/
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*$title = $form_state->getValue('item');
    if (strlen($title) < 5) {
      // Set an error for the form element with a key of "title".
      $form_state->setErrorByName('item', $this->t('The text must be at least 5 characters long.'));
    }*/
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('len_custom.lenovocustomconfig')
      ->set('content_publisher_update_url', $form_state->getValue('content_publisher_update_url'))
      ->set('content_publisher_delete_url', $form_state->getValue('content_publisher_delete_url'))
      ->set('content_publisher_update_url_staging', $form_state->getValue('content_publisher_update_url_staging'))
      ->set('content_publisher_delete_url_staging', $form_state->getValue('content_publisher_delete_url_staging'))
      ->save();
    $form_state->setRebuild();

  }

  public function promptCallback(array &$form, FormStateInterface $form_state) {
    // When dealing with forms submitted via Ajax we want to just return the
    // complete $form array. All of the logic for figuring out what should be in
    // the form is contained in buildForm().
    //
    // The one thing we will add here is any errors that were generated when
    // trying to validate the form. This snippet checks to see if there are any
    // errors, and if so creates a render array that will retrieve and display
    // the status messages, then renders it to HTML and tacks it onto the
    // beginning of the form.
    if ($form_state->hasAnyErrors()) {
      $renderer = \Drupal::service('renderer');
      $status_messages = ['#type' => 'status_messages'];
      $form['#prefix'] .= $renderer->renderRoot($status_messages);
    }

    return $form;
  }

}
